
README
--------------------------------------------------------------------------

This module allows any name to be entered in the "Authored By" form field in the
node edit form for nodes of type "Article".

If the name does not yet exist as a user name or as a Real Name then this module
automatically creates and sets as active a new user of role type "Open Author"
and designates the inputted name as the user name.

"Open Author" users by default have no access privileges, email accounts, or passwords.

This module was originally written for Drupal 7.34 by:

Kevin [dot] Bauer [at] ndsu.edu

INSTALLATION
--------------------------------------------------------------------------
1. Copy the open_author module folder to your modules directory.
2. Download, install, and enable the modules Real Name and Token.
3. Enable the module and the user type "Open Author" will be created automatically.
